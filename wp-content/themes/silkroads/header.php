<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package silkroads
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<!-- Contact dagarner (at) uchicago (dot) edu for questions regarding these Google Tag Manager and Analytics accounts. -->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MDQ9LV7');</script>
	<!-- End Google Tag Manager -->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MDQ9LV7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'silkroads' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			
				<div class="site-logo">
					<?php $logo = get_field("logo", "options"); ?>
					<a href="<?php echo site_url(); ?>" title="<?php get_bloginfo( 'name' ); ?> Home Link"><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['alt']; ?>" /></a>
				</div>
				
				<div class="site-title">
					<?php 
						$series_editor_url = get_field("series_editor_url", "options"); 
						$acquiring_editor_url = get_field("acquiring_editor_url", "options");
					?>
					<h1><span class="header-item"><?php the_field("site_title", "options"); ?></span><span class="header-item"><a href="<?php echo $series_editor_url["url"]; ?>" target="<?php echo $series_editor_url["target"]; ?>" title="<?php the_field("series_editor_name", "options"); ?> Bio"><?php the_field("series_editor_name", "options"); ?>, Series Editor</a></span><span class="header-item"><a href="<?php echo $acquiring_editor_url["url"]; ?>" target="<?php echo $acquiring_editor_url["target"]; ?>" title="<?php the_field("acquiring_editor_name", "options"); ?> Bio"><?php the_field("acquiring_editor_name", "options"); ?>, Acquiring Editor</a></span></h1>
				</div>

		</div><!-- .site-branding -->
		
		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars"></i></button>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</nav><!-- #site-navigation -->
		
	</header><!-- #masthead -->

	<div id="content" class="site-content">
