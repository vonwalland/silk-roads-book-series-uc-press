<?php
/**
 * Template Name: Home
 *
 * Template for the home page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package silkroads
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'home' );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
		
		<div class="book-list">
			<div class="book-section-title">Books</div>
			<div style="clear: both;"></div>
			
			<!-- BEGIN New WP query for book custom post types -->
			<?php
				query_posts(array( 
			        'post_type' => 'books',
			        'orderby' => 'date',
			        'order'   => 'DESC'
				) );
				while (have_posts()) : the_post();
			?>
			
			<div class="book-item-container">
				<div class="book-item">
					<?php $book_cover_image = get_field("book_cover_image"); ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<img class="book-item-image" src="<?php echo $book_cover_image['url']; ?>" alt="<?php echo $book_cover_image['alt']; ?>" title="<?php echo $book_cover_image['alt']; ?>" />
					</a>
					<div class="book-item-title"><?php the_title(); ?></div>
					<div class="book-item-editors"><?php the_field('book_editors'); ?></div>
					<div class="book-item-info"><?php the_field('book_information'); ?></div>
					<div class="book-item-url"><a href="<?php the_permalink(); ?>" title="More information on <?php the_title(); ?>">More Information</a></div>
				</div>
			</div>
			
			<?php 
				endwhile;
				wp_reset_query(); 
			?>
			<!-- END New WP query for book custom post types -->
		</div><!-- .book-list -->
		<div style="clear: both;"></div>
		
		<div class="email-subscribe">
			<h2>Sign up for notifications!</h2>
			<?php echo do_shortcode( '[mc4wp_form id="118"]' ); ?>
		</div>
		
		
		
	</div><!-- #primary -->

<?php
get_footer();
