(function($) {

	//Settings for lightbox
	var cbSettings = {
		rel: 'gallery',
		width: '95%',
		height: 'auto',
		maxWidth: '1000',
		maxHeight: 'auto',
		title: function() {
		return $(this).find('img').attr('alt');
		}
	}
	
	//Initialize jQuery Colorbox   
    $('.gallery-icon.landscape a[href$=".jpg"], .gallery-icon.landscape a[href$=".jpeg"],  .gallery-icon.landscape a[href$=".png"], .gallery-icon.landscape a[href$=".gif"]').colorbox(cbSettings);
    
    //Keep lightbox responsive on screen resize
	$(window).on('resize', function() {
		$.colorbox.resize({
			width: window.innerWidth > parseInt(cbSettings.maxWidth) ? cbSettings.maxWidth : cbSettings.width
		}); 
	});


})(jQuery);