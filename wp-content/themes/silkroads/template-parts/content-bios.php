<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package silkroads
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>	
	</header><!-- .entry-header -->

	<div class="entry-content">
		
		<div class="bio-picture">
			<?php $bio_picture = get_field("bio_picture"); ?>
			<img src="<?php echo $bio_picture['url']; ?>" alt="<?php echo $bio_picture['alt']; ?>" title="<?php echo $bio_picture['alt']; ?>" />
		</div>
				
		<div class="bio-title">
			<?php the_field('bio_title'); ?>
		</div>
		
		<div class="bio-biography">
			<h2>Biography</h2>
			<?php the_field('bio_biography'); ?>
		</div>
		
		<?php 
			$bio_education = get_field('bio_education');
			if ($bio_education):
		?>
		
		<div class="bio-education">
			<h2>Education</h2>
			<?php echo $bio_education; ?>
		</div>
		
		<?php endif; ?>
		
		<?php 
			$bio_publications = get_field('bio_publications');
			if ($bio_publications):
		?>
		
		<div class="bio-publications">
			<h2>Publications</h2>
			<?php echo $bio_publications; ?>
		</div>
		
		<?php endif; ?>
		
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
