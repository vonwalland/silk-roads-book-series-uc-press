<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package silkroads
 */

?>

<?php 
	$supplementary_images = get_field('book_supplementary_images', false, false);
	$supplementary_texts = get_field('book_supplementary_texts');
	
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>	
	</header><!-- .entry-header -->

	<div class="entry-content">
		
		<?php if ( is_singular() ) : ?>
		
		<div class="tab">
			
		  <button class="tablinks active" onclick="openBook(event, 'book-main')">Info</button>
		  
		  <button class="tablinks" onclick="openBook(event, 'book-toc')">Table of Contents</button>
		  
		  <?php if ($supplementary_texts): ?>
		  <button class="tablinks" onclick="openBook(event, 'book-supplementary-texts')">Supplementary Texts</button>
		  <?php endif; ?>
		  
		  <?php if ($supplementary_images): ?>
		  <button class="tablinks" onclick="openBook(event, 'book-supplementary-images')">Supplementary Images</button>
		  <?php endif; ?>
		  
		</div>
		
		<div id="book-main" class="tabcontent" style="display: block;">
		
			<div class="book-cover-image">
				<?php $book_cover_image = get_field("book_cover_image"); ?>
				<img src="<?php echo $book_cover_image['url']; ?>" alt="<?php echo $book_cover_image['alt']; ?>" title="<?php echo $book_cover_image['alt']; ?>" /></a>
			</div>
			
			<div class="book-description">
				<?php the_field('book_description'); ?>
			</div>
			
			<div class="book-editors">
				<span>Edited by:</span> <?php the_field('book_editors'); ?>
			</div>
			
			<div class="book-info">
				<span>Book Information:</span> <?php the_field('book_information'); ?>
			</div>
			
			<div class="book-url-link">
				<a target="_blank" href="<?php the_field('book_url'); ?>" title="Buy <?php the_title(); ?> Book">
					<button class="book-url">Buy Book</button>
				</a>
			</div>
			
		</div>
		
		<div id="book-toc" class="tabcontent">
			
			<div class="book-table-of-contents">
				<?php the_field('book_table_of_contents'); ?>
			</div>
			
		</div>
		
		<?php if ($supplementary_texts): ?>
		<div id="book-supplementary-texts" class="tabcontent">
			
			<div class="book-supplementary-texts">
				<?php the_field('book_supplementary_texts'); ?>
			</div>
			
		</div>
		<?php endif; ?>
		
		<?php if ($supplementary_images): ?>
		<div id="book-supplementary-images" class="tabcontent">
			
			<div class="book-supplementary-images">
				<?php

					$shortcode = '[gallery ids="' . implode(',', $supplementary_images) . '" link="file"]';
					echo do_shortcode( $shortcode );
					
				?>
			</div>
			
		</div>
		<?php endif; ?>
				
		<?php else : ?>
		
		<div class="book-cover-image">
			<?php $book_cover_image = get_field("book_cover_image"); ?>
			<a href="<?php get_permalink(); ?>" title="<?php the_title(); ?> Book"><img src="<?php echo $book_cover_image['url']; ?>" alt="<?php echo $book_cover_image['alt']; ?>" title="<?php echo $book_cover_image['alt']; ?>" /></a>
		</div>
		
		<div class="book-editors">
			<span>Edited by:</span> <?php the_field('book_editors'); ?>
		</div>
		
		<div class="book-description">
			<?php 
				$book_excerpt = wp_trim_words( get_field('book_description' ), $num_words = 50, $more = '...' ); 
				echo $book_excerpt;
			?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?> Book">Read more >></a>
		</div>
		
		<?php endif; ?>
		
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php silkroads_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
