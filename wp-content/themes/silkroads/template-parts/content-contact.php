<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package silkroads
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>
	</header><!-- .entry-header -->

	<?php silkroads_post_thumbnail(); ?>

	<div class="entry-content">
		<div class="series-editor">
			<h2 class="title">Series Editor</h2>
			<div class="name">
				<span><?php the_field("series_editor_name", "options"); ?></span>
			</div>
			<div class="url">
				<?php $series_editor_url = get_field("series_editor_url", "options"); ?>
				<a href="<?php echo $series_editor_url["url"]; ?>" target="<?php echo $series_editor_url["target"]; ?>" title="<?php the_field("series_editor_name", "options"); ?> Bio">Read Bio</a>
			</div>
			<div class="address">
				<?php the_field("series_editor_address", "options"); ?>
			</div>
			<div class="email">
				Email: <a href="mailto:<?php the_field("series_editor_email", "options"); ?>"><?php the_field("series_editor_email", "options"); ?></a>
			</div>
		</div>
		
		<div class="acquiring-editor">
			<h2 class="title">Acquiring Editor</h2>
			<div class="name">
				<span><?php the_field("acquiring_editor_name", "options"); ?></span>
			</div>
			<div class="url">
				<?php $acquiring_editor_url = get_field("acquiring_editor_url", "options"); ?>
				<a href="<?php echo $acquiring_editor_url["url"]; ?>" target="<?php echo $acquiring_editor_url["target"]; ?>" title="<?php the_field("acquiring_editor_name", "options"); ?> Bio">Read Bio</a>
			</div>
			<div class="address">
				<?php the_field("acquiring_editor_address", "options"); ?>
			</div>
			<div class="email">
				Email: <a href="mailto:<?php the_field("acquiring_editor_email", "options"); ?>"><?php the_field("acquiring_editor_email", "options"); ?></a>
			</div>
		</div>
		
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php silkroads_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
