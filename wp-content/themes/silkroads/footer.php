<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package silkroads
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		
		<?php 
			global $post;
			//If Contact page, don't put in the footer
			if( $post->ID != 49):	
		?>
		
		<div class="site-info">
			<div class="footer-header"><?php the_field("footer_header", "options"); ?></div>
			<div class="footer-contact-name"><?php the_field("series_editor_name", "options"); ?></div>
			<div class="footer-address"><?php the_field("series_editor_address", "options"); ?></div>
			<div class="footer-email">Email: <a href="mailto:<?php the_field("series_editor_email", "options"); ?>"><?php the_field("series_editor_email", "options"); ?></a></div>
		</div><!-- .site-info -->
		
		<?php endif; ?>
		
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
