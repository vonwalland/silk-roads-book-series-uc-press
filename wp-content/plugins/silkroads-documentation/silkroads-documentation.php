<?php
   /*
   Plugin Name: Silk Roads Website User Guide
   Plugin URI: http://silkroads.uchicago.edu
   description: A plugin to provide step by step help on adding content in the Silk Roads Theme.
   Version: 1.0
   Author: David A. Garner
   Author URI: https://www.davidagarner.com
   License: GPL2
   */



 
// Add a new top level menu link to the ACP
function srd_My_Admin_Link()
{
      add_menu_page(
        __('User Guide', 'textdomain'), // Title of the page
        'User Guide', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        'silkroads-documentation/srd-instructions.php', // The 'slug' - file to display when clicking the link
        '',
        'dashicons-editor-help',
        2
    );
}

// Hook the 'admin_menu' action hook, run the function named 'mfp_Add_My_Admin_Link()'
add_action( 'admin_menu', 'srd_My_Admin_Link' );


?>