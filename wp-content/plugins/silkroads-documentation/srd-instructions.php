<link rel='stylesheet' id='acf-global-css'  href='<?php echo plugins_url(); ?>/silkroads-documentation/srd-style.css' type='text/css' media='all' />

<div class="wrap srd">
	
	<!-- Main Section and TOC -->
	<h1>Silk Roads Website User Guide</h1>
	
	<h2>Table of Contents</h2>
	
	<ul>
		<li><a href="#1">Learn to use general features of WordPress.</a></li>
		<li><a href="#2">Edit the header, footer, and contact page.</a></li>
		<li><a href="#3">Edit the home page.</a></li>
		<li><a href="#4">Edit books, bios, or other pages.</a></li>
		<li><a href="#5">Edit navigation menu.</a></li>
		<li><a href="#6">Add new content.</a></li>
		<li><a href="#7">Send a newsletter/email blast with MailChimp.</a></li>
		<li><a href="#8">Smile if you're frustrated.</a></li>
	</ul>
	

	<!-- Section 1 -->
	<hr />
	
	<div id="ctn">
		<a name="1">&nbsp;</a>
		<h2>Learn to use general features of WordPress.</h2>
	</div>
	
	<p>WordPress is a popular content management system for websites. Because of this, it comes with tons of documentation on how to use its features. When in doubt, you can always consult the <a title="WordPress Codex" href="https://codex.wordpress.org/WordPress_Quick_Start_Guide" target="_blank">WordPress Codex</a> for general information. In addition to WordPress' own documentation, loads of other sites provide articles, videos, and images to help you learn WordPress. Here are some helpful guides.</p>
	
	<ul>
		<li><a title="Using the WordPress visual text editor" href="https://make.wordpress.org/support/user-manual/content/editors/visual-editor/" target="_blank">Using the WordPress visual text editor</a> (also known as "rich text editor" or "WYSIWYG editor" ["what-you-see-is-what-you-get"]).</li>
		<li><a title="Inserting images into the visual text editor" href="https://codex.wordpress.org/Inserting_Images_into_Posts_and_Pages" target="_blank">Inserting images into the visual text editor</a>.</li>
		<li><a title="Resetting your password" href="https://codex.wordpress.org/Resetting_Your_Password" target="_blank">Resetting your password</a>.</li>
		<li><a title="Using WordPress menus." href="https://codex.wordpress.org/Appearance_Menus_Screen" target="_blank">Using WordPress menus</a>.</li>
	</ul>
	
	<p><strong>Note about updating WordPress</strong>: Generally, updating WordPress software files (what makes the content management system work), plugins, and themes requires backing up the site, particularly it's database. Please leave the updates to the IT staff and/or me.</p>
	
	<p>There may be other things you need, or perhaps some WordPress features not enabled when the site was developed. For example, there is no "News" or "Blog" section of the site in which you can use "<strong>Posts</strong>, nor is there an archive page for "<strong>Bios</strong> like there is for "<strong>Books</strong>" (<a title="book archive page" target="_blank" href="<?php echo site_url(); ?>/books/">See Book archive page</a>). In addition to Ross and other tech staff, you can always reach out to me for help or changes. You have my email (<a href="mailto:dagarner@uchicago.edu">dagarner@uchicago.edu</a>) or you may call/text me at (865) 805-2917.</p>
		
			
	<!-- Section 2 -->
	<hr />
	
	<div id="ctn">
		<a name="2">&nbsp;</a>
		<h2>Edit the header, footer, and contact page.</h2>
	</div>
	
		<ul>
			<li>In the WordPress dashboard, click on the "<strong>Theme Options</strong>" button in the left-hand toolbar.</li>
			<li>Here you will find pieces of information that are used across the entire site (including the header, footer, and contact page).</li>
			<li>Edit the content you wish to change and, then, click "<strong>Update</strong>" in the right-hand side of the page to save your changes.</li>
		</ul>
		
	
	<!-- Section 3 -->
	<hr />
	
	<div id="ctn">
		<a name="3">&nbsp;</a>
		<h2>Edit the home page.</h2>
	</div>
	
		<ul>
			<li>In the WordPress dashboard, click on the "<strong>Pages</strong>" link in the left-hand menu.</li>
			<li>On the resulting page, find the page titled "<strong>Home</strong>" and click on it.</li>
			<li>Next, you can edit the main section of the home page by using the WordPress text editor. Be sure that you have clicked the "<strong>Visual</strong>" tab above the content to use the rich text editor.</li>
			<li>When finished, click the "<strong>Update</strong>" button on the right-hand side of the screen.</li>
			<li>The rest of the page is generated dynamically from the "<strong>Books</strong>" section of the WordPress dashboard.</li>
		</ul>
		
	
	<!-- Section 4 -->
	<hr />
	
	<div id="ctn">
		<a name="4">&nbsp;</a>
		<h2>Edit books, bios, or other pages.</h2>
	</div>
	
		<ul>
			<li>Editing books, bios, or other pages works much like editing the home page.</li>
			<li>In the WordPress dashboard, click on the item you wish to edit: "<strong>Books</strong>," "<strong>Bios</strong>," or "<strong>Pages</strong>." Unless we code changes to the site later, you will not need to use "<strong>Posts</strong>."</li>
			<li>On the resulting page, choose the particular book, bio, or page you wish to edit. The books and bios will include labeled fields for you to edit (with instructions on each).</li>
			<li>When finished, click on the "<strong>Update</strong>" button on the right-hand side to save your changes.</li>
		</ul>
		

	<!-- Section 5 -->
	<hr />
	
	<div id="ctn">
		<a name="5">&nbsp;</a>
		<h2>Edit navigation menu.</h2>
	</div>
	
		<ul>
			<li>To change the main navigation menu, either click on "<strong>Appearance</strong>" in the WordPress dashboard, then click "<strong>Menus</strong>" on the resulting page OR hover over "<strong>Appearance</strong>" and then choose "<strong>Menus</strong>."</li>
			<li>On the resulting page, on the left-hand side, tick the checkbox of the page, post, book, bio, custom link, or category you wish to add to the menu, then click the "<strong>Add to Menu</strong>" button.&nbsp;</li>
			<li>Next, you can drag the elements on the right-hand side to re-order the menu items as you wish.</li>
			<li>When finished, click "<strong>Save Menu</strong>" on the right-hand side of the page to save your changes.</li>
		</ul>
		
		
	<!-- Section 6 -->
	<hr />
	
	<div id="ctn">
		<a name="6">&nbsp;</a>
		<h2>Add new content.</h2>
	</div>
	
		<ul>
			<li>To add new content to the site, whether it's a page, bio, or book, the process generally works the same.</li>
			<li>In the WordPress dashboard, choose which type of content you want to add then click on "<strong>Pages</strong>," "<strong>Books</strong>," or "<strong>Bios</strong>." On the resulting page, click "<strong>Add New</strong>." You can also hover over any of those items and choose "<strong>Add New</strong>" as well.</li>
			<li>Next, fill out the appropriate fields and/or visual text editors. Books and Bios will include labeled fields with instructions.</li>
			<li>For search engine optimization purposes, be sure to fill out the section under the tab called "<strong>Yoast SEO</strong>." Instructions for editing that <a title="Yoast SEO Instructions" href="https://yoast.com/wordpress/plugins/seo/titles-and-meta-descriptions/" target="_blank">can be found here</a>.</li>
			<li>When done, you can either click the "<strong>Save Draft</strong>" button in the top right-hand section of the page, if you want to come back and edit more later, or you may click the "<strong>Publish</strong>" button to make the page live on the site.</li>
			<li>Remember, if you wish to add the page to the main navigation menu, be sure to add it under <strong>Appearance &gt; Menus</strong>.</li>
		</ul>


	<!-- Section 7 -->
	<hr />
	
	<div id="ctn">
		<a name="7">&nbsp;</a>
		<h2>Send a newsletter/email blast with MailChimp.</h2>
	</div>
	
		<ul>
			<li>For sending out a newsletter/email blast with MailChimp, detailed instructions <a title="MailChimp Instructions" href="Create%20a Regular Email Campaign: https://eepurl.com/dyilc1" target="_blank">can be found here at MailChimp</a>. Here, I will note a few differences.</li>
			<li>When choosing your list, be sure to choose the "<strong>Silk Roads Book Series Subscribers</strong>."</li>
			<li>When "designing" your email, be sure to choose the "<strong>Silks Roads Master Template</strong>" under the "<strong>Saved Templates</strong>" tab. This includes all the Silk Roads branding.</li>
			<li>You will fill out the template with your desired content by using MailChimp's drag-and-drop/WYSIWYG editor. Instructions on using that <a title="MailChimp instructions" href="https://mailchimp.com/help/use-boxed-text-content-blocks/" target="_blank">are found here at MailChimp</a>.</li>
			<li>When you are ready to test, at the top, under the "<strong>Preview and Test</strong>" tab, you can either preview your email in a browser or send a test email.&nbsp;<strong>NOTE</strong>: This test will not include certain global information found in the footer (like address, list description, etc.).&nbsp;</li>
			<li>Alternatively, you can do a "real" test by sending your campaign to the "<strong>Unveristy of Chicago Press</strong>" list, then copying your campaign afterwards, and changing the list. If you want to choose this method, be sure to add everyone to the list that you want to review it. Instructions for that <a title="mailchimp instructions" href="https://mailchimp.com/help/add-subscribers-to-groups/" target="_blank">are found here at MailChimp</a> (ignore the stuff about "Groups").</li>
		</ul>
	
	<!--Space to make the anchors a little easier to get to! -->
	<div id="filler"></div>
	
	<!--For funsies-->
	<div id="ctn">
		<a name="8">&nbsp;</a>
		<h2>:-)</h2>
	</div>
	<iframe src="https://giphy.com/embed/bAlYQOugzX9sY" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/end-full-bAlYQOugzX9sY">via GIPHY</a></p>
</div>